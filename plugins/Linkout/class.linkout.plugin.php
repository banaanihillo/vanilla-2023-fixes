<?php if (!defined('APPLICATION')) exit();
	
$PluginInfo['Linkout'] = array(
   'Name' => 'Link Out',
   'Description' => "Forces all outbound links to open in a new browser tab.",
   'Version' => '1.0.1',
   'License' => 'GNU GPL2', 
   'MobileFriendly' => TRUE,
   'RequiredApplications' => FALSE,
   'RequiredTheme' => FALSE, 
   'RequiredPlugins' => FALSE,
   'HasLocale' => FALSE,
   'RegisterPermissions' => FALSE,
   'Author' => 'Doobox Software',
   'AuthorEmail' => 'support@doobox.co.uk',
   'AuthorUrl' => 'https://www.doobox.co.uk'
);

class linkoutPlugin extends Gdn_Plugin {
	
	public function Base_Render_Before($sender) {
		$sender->addJsFile('linkout.js', 'plugins/Linkout');
	}
	
   public function Setup() {
      // Nothing to do here!
   }
   
   public function Structure() {
      // Nothing to do here!
   }
         
}
