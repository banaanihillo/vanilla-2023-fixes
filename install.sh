#!/bin/bash
set -e

sudo cp -r * /var/www/forum/

sudo rm -rf /var/www/forum/plugins/DiscussionPolls/locale/it-IT/
sudo rm -rf /var/www/forum/plugins/DiscussionPolls/locale/de-DE/
sudo rm -rf /var/www/forum/plugins/DiscussionPolls/locale/fi-FI/
sudo rm -rf /var/www/forum/plugins/DiscussionPolls/locale/fi/

sudo chown -R www-data:www-data /var/www/forum
# Required for puppeteer preview images to work
mkdir --parents /var/www/forum/uploads/images
sudo chown -R "$(whoami):$(whoami)" /var/www/forum/uploads/images/
sudo rm -rf /var/www/forum/cache/*
sudo service nginx reload
